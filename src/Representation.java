import javafx.scene.image.Image;
import javafx.scene.paint.Color;
// WritableImage

/**
 * (Vue) Classe abstraite des représentations. Contient la position,
 * le diamètre, l'image, et le statut de collision de l'objet correspondant
 * dans le modèle.
 */
public abstract class Representation {
    protected double[] pos;
    protected double diametre;
    protected Image image;
    protected boolean collision;

    /**
     * "Getter" qui retourne l'image de la représentation.
     * @return L'image de la représentation.
     */
    public final Image getImage(){
        return this.image;
    }

    /**
     * "Getter" qui retourne la position du coin supérieur gauche de
     * l'image de la représentation.
     * @return La position en question.
     */
    public final double[] getPos(){
        double[] copiePos = new double[2];
        copiePos[0] = this.pos[0];
        copiePos[1] = this.pos[1];
        return copiePos;
    }

    /**
     * "Setter" qui assigne la position du coin supérieur gauche de l'image.
     * @param nouvPos La nouvelle position.
     */
    public abstract void setPos(double[] nouvPos);

    /**
     * "Getter" qui retourne le diamètre de la représentation. Utile pour
     * le mode débug.
     * @return Le diamètre en question.
     */
    public final double getDiametre(){
        return this.diametre;
    }

    /**
     * "Setter" qui assigne l'état de collision de l'objet correspondant
     * du modèle. Utile pour le mode
     * débug.
     * @param collision L'état de collision.
     */
    public final void setCollision(boolean collision){
        this.collision = collision;
    }

    /**
     * "Getter" abstrait de la couleur de remplissage du cercle représentant
     * le fantôme et les obstacles dans le mode débug.
     * @return La couleur du cercle.
     */
    public abstract Color getCouleur();
}
