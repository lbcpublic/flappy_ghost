import javafx.scene.image.Image;
import javafx.scene.paint.Color;

/**
 * (Vue) Représentation d'un arrière-plan.
 */
public class RepArrierePlan extends Representation {
    private static String addresseImage = "/Images/bg.png";

    public RepArrierePlan(){
        this.pos = new double[2];
        this.diametre = 640; // Largeur de la fenetre / 2
        this.image = new Image(addresseImage);
    }

    /**
     * Aucune couleur pour l'arrière-plan.
     * @return null.
     */
    @Override
    public Color getCouleur(){
        return null;
    }

    /**
     * Assigne la position du coin supérieur gauche de l'image.
     * @param nouvPos La nouvelle position.
     */
    @Override
    public void setPos(double[] nouvPos) {
        this.pos[0] = nouvPos[0] - 320;
        this.pos[1] = nouvPos[1] - 200;
    }
}
