/**
 * (Modèle) Positionnable qui décrit le comportement d'un obstacle sinus .
 */
public class ObstacleSinus extends Obstacle {
    private double theta;
    private double y0;

    /**
     * Constructeur.
     * @param pos La position de l'obstacle.
     * @param rayon Le rayon de l'obstacle.
     */
    public ObstacleSinus(double[] pos, double rayon){
        super(pos,rayon);
        this.theta = 0.5*Math.PI - Math.PI*(Math.random());
        this.y0 = pos[1];
    }

    /**
     * Effectue un mouvement sinusoïdal.
     * @param dt Le délai en secondes depuis la dernière mise-à-jour du modèle.
     */
    @Override
    public void bouger(double dt){
        this.theta += 2*Math.PI*dt;
        this.pos[1] = this.y0 + 25*Math.sin(theta);
    }
}
