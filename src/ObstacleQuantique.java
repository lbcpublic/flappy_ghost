/**
 * (Modèle) Positionnable qui décrit le comportement d'un obstacle quantique.
 */
public class ObstacleQuantique extends Obstacle {

    /**
     * Constructeur.
     * @param pos La position de l'obstacle.
     * @param rayon Le rayon de l'obstacle.
     */
    public ObstacleQuantique(double[] pos, double rayon){
        super(pos,rayon);
    }

    /**
     * Effectue un saut quantique.
     */
    public void sautQuantique(){
        this.pos[0] += 60*(Math.random()-0.5);
        this.pos[1] += 60*(Math.random()-0.5);
    }
}
