/**
 * (Modèle) Positionnable qui décrit le comportement du fantôme.
 */
public class Fantome extends Positionnable {
    private static double graviteBase = 500;
    private static double incGravite = 15;
    private static double rFantome = 30;
    private static Modele modele;
    private double gravite;
    private double deltaPos;

    /**
     * Constructeur du fantôme.
     * @param pos Position du centre du fantôme.
     * @param modele Référence vers le modèle pour s'assurer que le fantôme reste sur
     *               les arrières-plans.
     */
    public Fantome(double[] pos, Modele modele){
        super(pos,new double[]{VXBase,0},rFantome);
        Fantome.modele = modele;
        this.gravite = graviteBase;
    }

    /**
     * Modifie la position et la vitesse, Gère les collisions avec le haut et le bas
     * du canevas.
     * @param dt Le délai en secondes depuis la dernière mise-à-jour du modèle.
     */
    public void bouger(double dt){
        this.deltaPos = dt*this.vit[0];
        this.pos[0] += this.deltaPos;
        modele.verifPos(this.pos[0]);

        this.pos[1] += this.vit[1]*dt + this.gravite*dt*dt/2;
        this.vit[1] += this.gravite*dt;

        if(this.vit[1] > 300){
            this.vit[1] = 300;
        }

        if (this.pos[1] < 30) {
            this.pos[1] = 30;
            this.vit[1] = -this.vit[1];
        }else if (this.pos[1] >= 370) {
            this.pos[1] = 370;
            this.vit[1] = -this.vit[1];
        }

    }

    /**
     * Ajuste la vitesse en Y du fantôme pour sauter.
     */
    public void sauter(){
        this.vit[1] = -300;
    }

    /**
     * Augmente la gravité.
     */
    public void incGravite(){
        this.gravite += incGravite;
    }

    /**
     * Réinialise la gravité à sa valeur par défaut.
     */
    public void reinitGravite(){
        this.gravite = graviteBase;
    }

    /**
     * "Getter" qui retourne le rayon du fantôme.
     * @return Le rayon du fantôme.
     */
    public static double getRFantome(){
        return rFantome;
    }

    /**
     * "Getter" qui retourne la variation de position du fantôme
     * depuis la dernière mise-à-jour.
     * @return La variation de position.
     */
    public double getDeltaPos(){
        return this.deltaPos;
    }
}
