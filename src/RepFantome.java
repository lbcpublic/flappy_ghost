import javafx.scene.image.Image;
import javafx.scene.paint.Color;

/**
 * (Vue) Représentation du fantôme.
 */
public class RepFantome extends Representation {
    private static Color couleurFantome = new Color(0.0,0.0,0.0,1);
    private static String addresseImage = "/Images/ghost.png";

    /**
     * Constructeur qui crée la représentation du fantôme.
     */
    public RepFantome(){
        this.pos = new double[2];
        this.diametre = 60;
        this.image = new Image(addresseImage);
    }

    /**
     * "Getter" abstrait de la couleur de remplissage du cercle représentant
     * le fantôme dans le mode débug.
     * @return La couleur du cercle.
     */
    @Override
    public Color getCouleur(){
        return couleurFantome;
    }

    /**
     * "Setter" qui assigne la position du coin supérieur gauche de l'image
     * du fantôme.
     * @param nouvPos La nouvelle position du fantôme.
     */
    @Override
    public void setPos(double[] nouvPos){
        this.pos[0] = nouvPos[0] - 30;
        this.pos[1] = nouvPos[1] - 30;
    }
}
