import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Separator;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import java.util.ArrayList;

/**
 * Classe principale de la Vue. Le coeur de l'application.
 */
public class FlappyGhost extends Application {
    private RepArrierePlan[] arrierePlans;
    private ArrayList<RepObstacle> obstacles;
    private RepFantome persPrinc;
    private Text score;
    private GraphicsContext contexte;
    private Canvas canevas;
    private boolean debug;
    private boolean pause;
    private double[] dimensions;
    private double posXCanevas;

    /**
     * Démarrage de l'application.
     * @param primaryStage La fenêtre du jeu.
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        this.arrierePlans = new RepArrierePlan[2];
        this.obstacles = new ArrayList<>();
        this.debug = false;
        this.pause = false;

        this.dimensions = new double[]{640.0,400.0};
        this.posXCanevas = 0.0;
        Controleur.setVue(this);
        Controleur controleur = new Controleur();

        VBox root = new VBox();
        Scene scene = new Scene(root,this.dimensions[0],this.dimensions[1] + 40);
        Canvas canvas = new Canvas(this.dimensions[0],this.dimensions[1]);
        this.canevas = canvas;
        this.contexte = canvas.getGraphicsContext2D();
        HBox options = new HBox();

        Button pause = new Button("Pause");
        CheckBox debug = new CheckBox("Mode debug");
        this.score = new Text("Score: 0");

        options.getChildren().add(pause);
        options.getChildren().add(new Separator(Orientation.VERTICAL));
        options.getChildren().add(debug);
        options.getChildren().add(new Separator(Orientation.VERTICAL));
        options.getChildren().add(this.score);
        options.setAlignment(Pos.CENTER);
        options.setSpacing(10);

        root.getChildren().add(canvas);
        root.getChildren().add(options);
        root.setSpacing(7);

        primaryStage.setScene(scene);

        FlappyGhost jeu = this;

        AnimationTimer chrono = new AnimationTimer(){
            private long passe = 0;

            @Override
            public void start(){
                passe = System.nanoTime();
                super.start();
            }

            @Override
            public void handle (long present){
                // dt en secondes
                double dt = (present - passe) * 1e-9;
                this.passe = present;
                controleur.mettreAJour(dt);
                jeu.dessinerJeu();
            }
        };

        canvas.setOnKeyPressed((event) -> {
            if(event.getCode() == KeyCode.SPACE && !this.pause){
                controleur.saut();
            }
        });

        debug.setOnMouseClicked((event) -> {
            controleur.setDebug(debug.isSelected());
            this.debug = debug.isSelected();
            jeu.dessinerJeu();

            /* Après l’exécution de la fonction, le
            focus va automatiquement au canvas */
            Platform.runLater(() -> {
                canvas.requestFocus();
            });
        });

        pause.setOnMouseClicked((event) -> {
            if(this.pause){
                chrono.start();
            }else{
                chrono.stop();
            }

            this.pause = !this.pause;

            /* Après l’exécution de la fonction, le
            focus va automatiquement au canvas */
            Platform.runLater(() -> {
                canvas.requestFocus();
            });
        });

        /* Lorsqu’on clique ailleurs sur la scène,
        le focus retourne sur le canvas */
        scene.setOnMouseClicked((event) -> {
            /* Après l’exécution de la fonction, le
            focus va automatiquement au canvas */
            Platform.runLater(() -> {
                canvas.requestFocus();
            });
        });

        primaryStage.setResizable(false);
        primaryStage.getIcons().add(persPrinc.getImage());
        primaryStage.setTitle("Flappy Ghost");
        primaryStage.show();
        chrono.start();
        canvas.requestFocus();
    }

    /**
     * Remplit le canevas avec toutes les formes et images nécessaires.
     */
    public void dessinerJeu(){
        this.contexte.clearRect(-this.dimensions[0],0,2*this.dimensions[0],this.dimensions[1]);
        this.contexte.translate(-this.posXCanevas,0.0);

        double[] pos = this.arrierePlans[0].getPos();
        this.contexte.drawImage(this.arrierePlans[0].getImage(),pos[0],pos[1]);
        pos = this.arrierePlans[1].getPos();
        this.contexte.drawImage(this.arrierePlans[1].getImage(),pos[0],pos[1]);

        if(this.debug){
            double diametre;

            for(RepObstacle obstacle : this.obstacles){
                this.contexte.setFill(obstacle.getCouleur());
                pos = obstacle.getPos();
                diametre = obstacle.getDiametre();
                this.contexte.fillOval(pos[0],pos[1],diametre,diametre);
            }

            this.contexte.setFill(persPrinc.getCouleur());
            pos = this.persPrinc.getPos();
            diametre = persPrinc.getDiametre();
            this.contexte.fillOval(pos[0],pos[1],diametre,diametre);

        }else{
            for(RepObstacle obstacle : this.obstacles){
                pos = obstacle.getPos();
                this.contexte.drawImage(obstacle.getImage(),pos[0],pos[1]);
            }

            pos = this.persPrinc.getPos();
            this.contexte.drawImage(this.persPrinc.getImage(),pos[0],pos[1]);
        }

        this.contexte.translate(this.posXCanevas,0.0);
    }

    /**
     * Crée une représentation de fantôme.
     * @return La représentation du fantôme.
     */
    public Representation creerImageFantome(){
        this.persPrinc = new RepFantome();
        return this.persPrinc;
    }

    /**
     * Crée deux représentations d'arrière-plan, avec les positions appropriées.
     */
    public void creerImagesArrierePlan(){
        this.arrierePlans[0] = new RepArrierePlan();
        this.arrierePlans[0].setPos(new double[]{this.dimensions[0]/2,this.dimensions[1]/2});
        this.arrierePlans[1] = new RepArrierePlan();
        this.arrierePlans[1].setPos(new double[]{3*this.dimensions[0]/2,this.dimensions[1]/2});
    }

    /**
     * Crée une représentation d'obstacle.
     * @param rayon Rayon de l'obstacle.
     * @return La représentation de l'obstacle créé.
     */
    public Representation creerImageObstacle(double rayon){
        RepObstacle obstacle = new RepObstacle(rayon);
        this.obstacles.add(obstacle);
        return obstacle;
    }

    /**
     * Supprime la représentation d'un obstacle.
     * @param rep La représentation de l'obstacle à supprimer.
     */
    public void suppImageObstacle(Representation rep){
        this.obstacles.remove(rep);
    }

    /**
     * Démarre l'application.
     * @param args Aucun.
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Affiche le score sur la fenêtre.
     * @param score
     */
    public void setScore(int score){
        this.score.setText("Score: " + score);
    }

    /**
     * "Getter" qui retourne les dimensions de la fenêtre.
     * @return Les dimensions de la fenêtre.
     */
    public double[] getDimensions(){
        double[] copieDim = new double[2];
        copieDim[0] = this.dimensions[0];
        copieDim[1] = this.dimensions[1];
        return copieDim;
    }

    /**
     * Déplaclement en X du centre effectif du canevas.
     * @param deltaX Le déplacement en question.
     */
    public void deplacerCanevas(double deltaX){
        this.posXCanevas += deltaX;
    }
}
