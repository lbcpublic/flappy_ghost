import java.util.Hashtable;
import java.util.ArrayList;

/**
 * Classe Controleur
 * Fait le lien entre le modèle et la vue.
 */
public class Controleur {
    private static FlappyGhost vue;
    private static Modele modele;
    private Hashtable<Positionnable,Representation> representations;
    private ArrayList<Obstacle> obstacles;
    private Fantome persPrinc;

    /**
     * Conctructeur du controleur.
     * Initialise le modèle, le fantôme et les images d'arrière plan.
     */
    public Controleur(){
        modele = new Modele(vue.getDimensions());
        Modele.setControleur(this);
        this.representations = new Hashtable<>();
        this.obstacles = new ArrayList<>();

        this.persPrinc = modele.creerFantome();
        Representation imgFantome = vue.creerImageFantome();
        imgFantome.setPos(this.persPrinc.getPos());
        this.representations.put(this.persPrinc, imgFantome);
        vue.creerImagesArrierePlan();
    }

    /**
     * Détermine la vue de la classe Controleur.
     * @param nouvVue La vue.
     */
    public static void setVue(FlappyGhost nouvVue){
        vue = nouvVue;
    }

    /**
     * Transmet le nouveau statut de débogage au modèle.
     * @param newDebug Le nouveau statut.
     */
    public void setDebug(boolean newDebug){
        modele.setDebug(newDebug);
    }

    /**
     * Met à jour le modèle et les positions des représentations.
     * @param dt Le temps écoulé (en s) depuis la dernière mise à jour.
     */
    public void mettreAJour(double dt){ // dt en secondes
        modele.mettreAJour(dt);

        for(int i = 0; i < this.obstacles.size(); i++){
            Obstacle obstacle = this.obstacles.get(i);
            this.representations.get(obstacle).setPos(obstacle.getPos());
        }

        this.representations.get(this.persPrinc).setPos(this.persPrinc.getPos());

        vue.deplacerCanevas(this.persPrinc.getDeltaPos());
    }

    /**
     * Dirige la création d'un obstacle dans le modèle et dans la vue
     * et relie positionnable et représentation.
     */
    public void creerObstacle(){
        Obstacle obstacle = modele.creerObstacle();
        // Créer modèle, représentation et associer les deux.
        this.obstacles.add(obstacle);
        Representation rep = vue.creerImageObstacle(obstacle.getRayon());
        this.representations.put(obstacle, rep);
        rep.setPos(obstacle.getPos());
    }

    /**
     * Met en marche la suite d'opéartions a effectuer lors d'une collision
     * qui se produit hors du mode debug.
     */
    public void defaite(){
        while(this.obstacles.size() != 0){
            this.suppObstacle(this.obstacles.get(this.obstacles.size()-1));
        }

        vue.setScore(0);
    }

    /**
     * Dirige la suppression d'un obstacle dans le modèle et dans la vue
     * de façon à supprimer toute référence vers l'obstacle supprimé.
     * @param obstacle L'obstacle à supprimer.
     */
    public void suppObstacle(Obstacle obstacle){
        modele.suppObstacle(obstacle);
        vue.suppImageObstacle(this.representations.get(obstacle));
        this.representations.remove(obstacle);
        this.obstacles.remove(obstacle); // ???
    }

    /**
     * Transmet l'instruction de faire sauter le fantôme au modèle.
     */
    public void saut(){
        modele.sauter();
    }

    /**
     * Transmet l'état de collision à l'image d'un obstacle.
     * @param obstacle Obstacle dont l'état doit être transmis.
     * @param collision L'état à transmettre.
     */
    public void collision(Obstacle obstacle, boolean collision){
        this.representations.get(obstacle).setCollision(collision);
    }

    /**
     * Transmet l'information sur le score à la vue.
     * @param score Le score à transmettre.
     */
    public void passerScore(int score){
        vue.setScore(score);
    }

    /**
     * Centre la position de la vue sur le fantôme.
     * @param deltaPos Le déplacement du fantôme lors de la mise à jour.
     */
    public void ajusterCanevas(double deltaPos){
        vue.deplacerCanevas(deltaPos);
    }
}
