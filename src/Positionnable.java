/**
 * (Modèle) Classe abstraite des objets ayant une position, un rayon
 * et une vitesse.
 */
public abstract class Positionnable {
    protected static double VXBase = 120;
    protected static double incVX = 15;
    protected double[] pos;
    protected double rayon;
    protected double[] vit;
    protected boolean aDepasser;

    /**
     * Constructeur parent des objets ayant une position, une vitesse et
     * un rayon.
     * @param pos La position.
     * @param vit La vitesse.
     * @param rayon Le rayon.
     */
    public Positionnable(double[] pos, double[] vit, double rayon){
        double[] posInit = new double[2];
        posInit[0] = pos[0];
        posInit[1] = pos[1];
        this.pos = posInit;
        double[] vitInit = new double[2];
        vitInit[0] = vit[0];
        vitInit[1] = vit[1];
        this.vit = vitInit;
        this.rayon = rayon;
        this.aDepasser = true;
    }

    /**
     * "Getter" qui retourne le rayon de l'objet.
     * @return Le rayon.
     */
    public final double getRayon(){
        return this.rayon;
    }

    /**
     * "Getter" qui retourne la position de l'objet.
     * @return La position.
     */
    public final double[] getPos(){
        double[] copiePos = new double[2];
        copiePos[0] = this.pos[0];
        copiePos[1] = this.pos[1];
        return copiePos;
    }

    /**
     * Modifie la position et la vitesse des objets.
     * @param dt Le délai en secondes depuis la dernière mise-à-jour du modèle.
     */
    public abstract void bouger(double dt);

    /**
     * Augmente la vitesse en X de l'objet jusqu'à une limite de 300.
     */
    public final void incVX(){
        this.vit[0] += incVX;
         if(this.vit[0] > 300.0){
             this.vit[0] = 300.0;
         }
    }

    /**
     * Incrémente la position en X de l'objet du modèle.
     * @param inc Incrément de la position.
     */
    public final void incPosX(double inc){
        this.pos[0] += inc;
    }

    /**
     * Remet la vitesse en x de l'objet à sa valeur par défaut.
     */
    public final void reinitVX(){
        this.vit[0] = VXBase;
    }

    /**
     * Indique s'il y a eu une collision entre 2 positionnables.
     * @param a Premier positionnable.
     * @param b Deuxième positionnable.
     * @return true s'il y a collision; false sinon.
     */
    public static boolean collision(Positionnable a, Positionnable b){
        return Positionnable.distance(a,b) < a.rayon + b.rayon;
    }

    /**
     * Détermine la distance entre 2 positionnables.
     * @param a Premier positionnable.
     * @param b Deuxième positionnable.
     * @return La distance.
     */
    public static double distance(Positionnable a, Positionnable b){
        double deltaX = a.pos[0] - b.pos[0];
        double deltaY = a.pos[1] - b.pos[1];
        return Math.sqrt((deltaX)*(deltaX) + (deltaY)*(deltaY));
    }
}
