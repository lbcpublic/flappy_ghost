/**
 * (Modèle) Positionnable qui décrit le comportement d'un obstacle standard.
 */
public class Obstacle extends Positionnable {

    /**
     * Constructeur.
     * @param pos La position de l'obstacle.
     * @param rayon Le rayon de l'obstacle.
     */
    public Obstacle(double[] pos, double rayon){
        super(pos,new double[2],rayon);
    }

    /**
     * Indique que l'obstacle n'est plus à dépasser.
     */
    public void depasse(){
        this.aDepasser = false;
    }

    /**
     * "Getter" qui indique si l'obstacle est toujours à dépasser.
     * @return
     */
    public final boolean aDepasser(){
        return this.aDepasser;
    }

    /**
     * Ne fait rien.
     * @param dt Le délai en secondes depuis la dernière mise-à-jour du modèle.
     */
    @Override
    public void bouger(double dt){

    }
}
