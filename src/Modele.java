import java.util.ArrayList;

/**
 * Classe qui calcule et transmet l'information relative à la physique
 * du jeu au contrôleur et au reste du modèle.
 */
public class Modele {
    private static Controleur controleur;
    private ArrayList<Obstacle> obstacles;
    private Fantome persPrinc;
    private double delaiObstacle;
    private double delaiQuantique;
    private int score;
    private boolean debug;
    private double[] dimensions;
    private double xMax;

    /**
     * Constructeur qui initialise les paramètres numériques et les
     * conteneurs du modèle.
     * @param dimensions Les dimensions du canevas.
     */
    public Modele(double[] dimensions){
        this.obstacles = new ArrayList<>();
        this.delaiObstacle = 0.0;
        this.delaiQuantique = 0.0;
        this.score = 0;
        this.debug = false;
        this.dimensions = new double[2];
        this.dimensions[0] = dimensions[0];
        this.xMax = 3*dimensions[0]/2;
        this.dimensions[1] = dimensions[1];
    }

    /**
     * Setteur qui donne une référence du contrôleur à la classe modèle.
     * @param controleur Le contrôleur du jeu.
     */
    public static void setControleur(Controleur controleur) {
        Modele.controleur = controleur;
    }

    /**
     * Effectue les opérations nécessaires à la création d'une instance de fantôme.
     * @return Le fantôme créé.
     */
    public Fantome creerFantome(){
        double[] fantomePos = new double[]{this.dimensions[0]/2,this.dimensions[1] - Fantome.getRFantome()};
        this.persPrinc = new Fantome(fantomePos, this);

        return this.persPrinc;
    }

    /**
     * Met à jour les positions, les vitesses, le score et les états de collision
     * au reste du modèle.
     * @param dt Temps écoulé (en secondes) depuis la dernière mise-à-jour.
     */
    public void mettreAJour(double dt){ // dt en secondes
        this.delaiObstacle += dt;
        this.delaiQuantique += dt;

        this.miseAJourPos(dt);
        if(this.verifCollisions() && !this.debug){
            this.persPrinc.reinitGravite();
            this.persPrinc.reinitVX();

            this.score = 0;

            this.delaiObstacle = 0.0;
            this.delaiQuantique = 0.0;

            controleur.defaite();
        }

        // Mettre score à jour et l'attribut des obstacles
        this.miseAJourScore();

        if(this.delaiObstacle >= 3.0){
            this.delaiObstacle = 0.0;
            controleur.creerObstacle();
        }
    }

    /**
     * Met à jour les positions des objets du modèle et en ordonne la destruction
     * lorsque c'est nécessaire.
     * @param dt Temps écoulé (en secondes) depuis la dernière mise-à-jour.
     */
    private void miseAJourPos(double dt){
        if(this.delaiQuantique >= 0.2){
            this.delaiQuantique = 0.0;

            for(int i = 0; i < this.obstacles.size(); i++){
                Obstacle obstacle = this.obstacles.get(i);
                if(obstacle instanceof ObstacleQuantique){
                    ((ObstacleQuantique) obstacle).sautQuantique();
                }
            }
        }

        for(int i = 0; i < this.obstacles.size(); i++){
            Obstacle obstacle = this.obstacles.get(i);
            obstacle.bouger(dt);

            if(this.persPrinc.getPos()[0] - (obstacle.getPos()[0] + obstacle.getRayon()) > this.dimensions[0]/2){
                controleur.suppObstacle(obstacle);
            }
        }

        this.persPrinc.bouger(dt);
    }

    /**
     * Vérifie la présence de collision entre le fantôme et les obstacles, et
     * met à jour l'état de collision si nécessaire.
     * @return true s'il y a eu une collision; false sinon.
     */
    private boolean verifCollisions(){
        boolean collision = false;

        for(int i = 0; i < this.obstacles.size(); i++){
            Obstacle obstacle = this.obstacles.get(i);

            if(Positionnable.collision(this.persPrinc,obstacle)){
                collision = true;
                if(this.debug){
                    controleur.collision(obstacle, true);
                }
            }else if(this.debug){
                controleur.collision(obstacle, false);
            }
        }

        return collision;
    }

    /**
     * Augmente le score lors d'un premier dépassement d'un objet par le fantôme,
     * et met à jour les vitesses et la gravité lorsque c'est nécessaire.
     */
    private void miseAJourScore(){
        double[] fantomePos = persPrinc.getPos();

        for(int i = 0; i < this.obstacles.size(); i++){
            Obstacle obstacle = this.obstacles.get(i);

            double[] pos = obstacle.getPos();

            if(pos[0] < fantomePos[0] && obstacle.aDepasser()){
                obstacle.depasse();
                this.score++;
                controleur.passerScore(this.score*5);

                if((this.score & 1) == 0){
                    this.persPrinc.incVX();
                    persPrinc.incGravite();
                }
            }
        }
    }

    /**
     * Effectue les opérations nécessaires à la création d'une instance d'obstacle.
     * @return la référence vers l'obstacle créé.
     */
    public Obstacle creerObstacle(){
        Obstacle obstacle;

        int typeObstacle = (int)(3.0*Math.random());

        double rayon = (28 + 18*(2*Math.random() - 1));
        double[] pos = new double[]{this.persPrinc.getPos()[0] + this.dimensions[0]/2 + rayon,
                rayon + (this.dimensions[1] - 2*rayon)*Math.random()};

        switch (typeObstacle){
            case 0: obstacle = new Obstacle(pos,rayon); break; // Position, vitesse, rayon
            case 1: obstacle = new ObstacleSinus(pos,rayon); break;
            case 2: obstacle = new ObstacleQuantique(pos,rayon); break;
            default: obstacle = new Obstacle(pos,rayon); break;
        }

        this.obstacles.add(obstacle);

        return obstacle;
    }

    /**
     * Retire l'obstacle du conteneur d'obstacle.
     * @param obstacle L'obstacle à retirer.
     */
    public void suppObstacle(Obstacle obstacle){
        this.obstacles.remove(obstacle);
    }

    /**
     * Commande un saut au fantôme.
     */
    public void sauter(){
        this.persPrinc.sauter();
    }

    /**
     * Met à jour l'état de débogage.
     * @param newDebug le nouvel état de débogage.
     */
    public void setDebug(boolean newDebug){
        this.debug = newDebug;
    }

    /**
     * S'assure que le fantôme et les obstacles ne dépassent pas les
     * images d'arrière-plan. Donne l'illusion de continuité au joueur.
     * @param posXFantome La position en X du fantôme.
     */
    public void verifPos(double posXFantome){
        if(posXFantome > this.xMax){
            double inc = -this.dimensions[0];
            this.persPrinc.incPosX(inc);
            for(int i = 0; i < this.obstacles.size(); i++){
                this.obstacles.get(i).incPosX(inc);
            }
            controleur.ajusterCanevas(-this.dimensions[0]);
        }
    }
}
