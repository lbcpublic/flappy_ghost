import javafx.scene.image.Image;
import javafx.scene.paint.Color;

/**
 * (Vue) Représentation d'un obstacle.
 */
public class RepObstacle extends Representation {
    private static String[] images;
    private static Color couleurObstacle = new Color(1.0,1.0,0.0,1);
    private static Color couleurCollision = new Color(1.0,0.0,0.0,1);

    /**
     * Constructeur qui fabrique la représentation d'un obstacle.
     * @param rayon Le rayon (en pixel) qu'aura l'obstacle.
     */
    public RepObstacle(double rayon){
        if(images == null){
            images = new String[27];

            for(int i = 0; i < 27; i++){
                // Format des images : 90px X 90px
                // Déterminer aléatoirement le format affiché
                images[i] = "/Images/obstacles/" + i + ".png";
            }
        }
        this.pos = new double[2];
        this.diametre = 2*rayon;
        this.image = new Image(images[(int)(27.0*Math.random())]
                ,this.diametre,this.diametre,true,false);
        this.collision = false;
    }

    /**
     * "Getter" qui retourne la couleur de l'obstacle. Utile en mode débug.
     * @return La couleur de l'obstacle.
     */
    public Color getCouleur(){
        if(this.collision){
            return couleurCollision;
        }else{
            return couleurObstacle;
        }
    }

    /**
     *  "Setter" qui assigne la position de la représentation de l'obstacle .
     */
    @Override
    public void setPos(double[] nouvPos) {
        this.pos[0] = nouvPos[0] - this.diametre/2;
        this.pos[1] = nouvPos[1] - this.diametre/2;
    }
}
